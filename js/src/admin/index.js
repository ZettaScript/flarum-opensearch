app.initializers.add('tuxmain/opensearch', function(app) {
  app.extensionData
  .for("tuxmain-opensearch")
  .registerSetting(
    {
      setting: "tuxmain-opensearch.title",
      label: app.translator.trans("tuxmain-opensearch.admin.title"),
      type: "text",
    }
  )
  .registerSetting(
    {
      setting: "tuxmain-opensearch.description",
      label: app.translator.trans("tuxmain-opensearch.admin.description"),
      type: "text",
    }
  )
  .registerSetting(
    {
      setting: "tuxmain-opensearch.url",
      label: app.translator.trans("tuxmain-opensearch.admin.url"),
      help: app.translator.trans("tuxmain-opensearch.admin.url_help"),
      type: "text",
    }
  )
});
