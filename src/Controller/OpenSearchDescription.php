<?php
namespace Tuxmain\OpenSearch\Controller;

use Flarum\Settings\SettingsRepositoryInterface;
use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class OpenSearchDescription
 * @package V17Development\FlarumSeo\Controller
 */
class OpenSearchDescription implements RequestHandlerInterface
{
    protected $settings;

    /**
     * OpenSearchDescription constructor.
     * @param SettingsRepositoryInterface $settings
     */
    public function __construct(SettingsRepositoryInterface $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param ServerRequestInterface $request
     * @return mixed
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $favicon_path = $this->settings->get('favicon_path');
        if($favicon_path)
            $favicon_path = 'assets/' . $favicon_path;
        
        $response = new Response();
        $response->getBody()->write('<?xml version="1.0" encoding="utf-8"?><OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/" xmlns:moz="http://www.mozilla.org/2006/browser/search/">
		<ShortName>'.$this->settings->get('tuxmain-opensearch.title', $this->settings->get('forum_title')).'</ShortName>
		<Description>'.$this->settings->get('tuxmain-opensearch.description', $this->settings->get('forum_description')).'</Description>
        <InputEncoding>UTF-8</InputEncoding>'
        .($favicon_path?
            '<Image height="16" width="16">data:image/'.pathinfo($favicon_path, PATHINFO_EXTENSION).';base64,'.base64_encode(file_get_contents($favicon_path)).'</Image>'
            :''
		).'<Url type="text/html" method="get" template="'.$this->settings->get('tuxmain-opensearch.url').'"/>
	</OpenSearchDescription>');
        return $response->withHeader('Content-Type', 'application/opensearchdescription+xml');
    }
}