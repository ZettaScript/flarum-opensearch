<?php
namespace Tuxmain\OpenSearch\Listener;

use Flarum\Frontend\Document;
use Flarum\Settings\SettingsRepositoryInterface;
use Psr\Http\Message\ServerRequestInterface;

class PageListener
{
    /**
     * @var SettingsRepositoryInterface
     */
    protected $settings;

    public function __construct(SettingsRepositoryInterface $settings)
    {
        $this->settings = $settings;
	}
	
	/**
     * Get current Flarum document and current Server Request
     *
     * @param Document $flarumDocument
     * @param ServerRequestInterface $serverRequestInterface
     */
    public function __invoke(Document $flarumDocument, ServerRequestInterface $serverRequestInterface)
    {
        $flarumDocument->head[] = '<link title="'.htmlspecialchars($this->settings->get('tuxmain-opensearch.title', $this->settings->get('forum_title'))).'" type="application/opensearchdescription+xml" rel="search" href="/opensearch.xml"/>';
    }
}
