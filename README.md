# OpenSearch

AGPLv3 [![Latest Stable Version](https://img.shields.io/packagist/v/tuxmain/opensearch.svg)](https://packagist.org/packages/tuxmain/opensearch)

A [Flarum](http://flarum.org) extension for integrating your forum search in browser searchbar, using the [OpenSearch](https://en.wikipedia.org/wiki/OpenSearch) standard.

### Installation

```sh
composer require tuxmain/opensearch
```

### Updating

```sh
composer update tuxmain/opensearch
```

### Contribute

TODO:

* Suggestions
* Static `opensearch.xml` file generation

Translations are welcome. Post them on the forum if you do not have a Framagit account.

### Links

- [Forum](https://discuss.flarum.org/d/27367)
- [Packagist](https://packagist.org/packages/tuxmain/opensearch)
- [GitLab](https://framagit.org/ZettaScript/flarum-opensearch)
- [LiberaPay](https://liberapay.com/tuxmain/)
