<?php

/*
 * This file is part of tuxmain/opensearch.
 *
 * CopyLeft 2021 Pascal Engélibert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tuxmain\OpenSearch;

use Flarum\Extend;
use Tuxmain\OpenSearch\Controller\OpenSearchDescription;
use Tuxmain\OpenSearch\Listener\PageListener;

return [
    (new Extend\Frontend('forum'))
        ->content(PageListener::class),
	(new Extend\Routes('forum'))
	    ->get('/opensearch.xml', 'tuxmain-opensearch', OpenSearchDescription::class),
    (new Extend\Frontend('admin'))
        ->js(__DIR__.'/js/dist/admin.js'),
    new Extend\Locales(__DIR__ . '/resources/locale')
];
